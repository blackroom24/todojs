const todoInput = document.querySelector(".todo-input");
const addButton = document.querySelector(".add-button");
const todoList = document.querySelector(".todo-list");
const filter = document.querySelector(".filter-list");

document.addEventListener("DOMContentLoaded", getList);
addButton.addEventListener("click", addTask);
todoList.addEventListener("click", deleteCheck);
filter.addEventListener("change", filterList);

function createComponents(value) {
  // making todo div
  const todoDiv = document.createElement("div");
  todoDiv.classList.add("todo");

  const newTask = document.createElement("li");
  newTask.innerText = value;
  newTask.classList.add("task");
  todoDiv.appendChild(newTask);

  const completedButton = document.createElement("button");
  completedButton.innerHTML = "<i class='fas fa-check'></i>";
  completedButton.classList.add("check-btn");
  todoDiv.appendChild(completedButton);

  const trashButton = document.createElement("button");
  trashButton.innerHTML = "<i class='fas fa-trash'></i>";
  trashButton.classList.add("trash-btn");
  todoDiv.appendChild(trashButton);

  todoList.appendChild(todoDiv);
}
function addTask(e) {
  e.preventDefault();
  if (!todoInput.value) return;
  createComponents(todoInput.value);
  saveLocalStorage(todoInput.value);
  todoInput.value = "";
}
function deleteCheck(e) {
  const item = e.target;
  const todo = item.parentElement;
  if (item.classList[0] === "trash-btn") {
    todo.classList.add("fall");
    todo.addEventListener("animationend", function () {
      removeLocalStorage(todo);
      todo.remove();
    });
  }
  if (item.classList[0] === "check-btn") todo.classList.toggle("completed");
}
function filterList(e) {
  const value = e.target.value;
  const todos = todoList.childNodes;
  console.log(value);
  todos.forEach(function (todo) {
    switch (value) {
      case "all":
        todo.style.display = "flex";
        break;
      case "completed":
        if (todo.classList.contains("completed")) {
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }

        break;
      case "pending":
        if (!todo.classList.contains("completed")) {
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }
        break;
      default:
        break;
    }
  });
}
function saveLocalStorage(todo) {
  let todos;
  if (localStorage.getItem("todos") !== null) {
    todos = JSON.parse(localStorage.getItem("todos"));
  } else {
    todos = [];
  }
  todos.push(todo);
  localStorage.setItem("todos", JSON.stringify(todos));
}
function getList() {
  let todos;
  if (localStorage.getItem("todos") !== null) {
    todos = JSON.parse(localStorage.getItem("todos"));
  } else {
    todos = [];
  }
  todos.forEach(function (todo) {
    createComponents(todo);
  });
}
function removeLocalStorage(todo) {
  let todos;
  if (localStorage.getItem("todos") !== null) {
    todos = JSON.parse(localStorage.getItem("todos"));
  } else {
    todos = [];
  }
  const todoIndex = todo.children[0].innerText;
  todos.splice(todos.indexOf(todoIndex), 1);
  localStorage.setItem("todos", JSON.stringify(todos));
}
